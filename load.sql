/*
    drop table if exists AnalystArticle;
    drop table if exists PartnerArticle;
    drop table if exists Article;
    drop table if exists IPODetails;
    drop table if exists Financials;
    drop table if exists StockComment;
    drop table if exists StockPrice;
    drop table if exists Stock;



*/



/* Create strong entity Stock table which holds symbols, will be populated by the csvs later on */
create table Stock (
	symbol varchar(6),
	primary key (symbol)
);

/********************************************************************************************/
/* LOAD PRICING DATA */

/* Create stock price table */ 
create table StockPrice (
    date datetime,
    volume decimal(12, 0),
	open decimal(22, 12),
	high decimal(22, 12),
	low decimal(22, 12),
	close decimal(22, 12),
	adjClose decimal(22, 12),
	symbol varchar(6),
    
    primary key (symbol, date)
);

load data infile '/var/lib/mysql-files/18-Stocks/fh_5yrs.csv' ignore into table StockPrice
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;

/* Populate Stock table using the pricing data */
insert into Stock (symbol)
select distinct symbol
from StockPrice;

/* Add the foreign key constraint */
alter table StockPrice
add foreign key (symbol) references Stock(symbol);

/********************************************************************************************/


/********************************************************************************************/
/* LOAD FINANCIAL DATA */

/* Create financials table */
create table Financials(
	symbol varchar(6),
    year int,
    revenue decimal(16, 2),
    revenueGrowth decimal(16, 2),
    costOfRevenue decimal(16, 2),
    grossProfit decimal(14, 2),
    rndExpenses decimal(14, 2),
    eps decimal(14, 4),
    epsDiluted decimal(14, 4),
    dividendPerShare decimal(17, 4),
    ebitda decimal(14, 2),
    ebit decimal(14, 2),
    profitMargin decimal(17, 4),
    cashAndEquivalent decimal(14, 2),
    debtGrowth decimal(10, 4),
    
    primary key (symbol, year)
);

/* Create temporary financials table for loading data */
create table FinancialsTemp(
	symbol varchar(6),
    revenue decimal(16, 2),
    revenueGrowth decimal(16, 2),
    costOfRevenue decimal(16, 2),
    grossProfit decimal(14, 2),
    rndExpenses decimal(14, 2),
    eps decimal(14, 4),
    epsDiluted decimal(14, 4),
    dividendPerShare decimal(17, 4),
    ebitda decimal(14, 2),
    ebit decimal(14, 2),
    profitMargin decimal(17, 4),
    cashAndEquivalent decimal(14, 2),
    debtGrowth decimal(10, 4)
);

/* Load 2014 financial data into db */
load data infile '/var/lib/mysql-files/Group41/2014_Financial_Data_processed_G41.csv' ignore into table FinancialsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Move 2014 financial data into correct location */
insert into Financials(symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)
select symbol, 2014, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth
from FinancialsTemp;
/* Empty the temp table */
truncate FinancialsTemp;

/* Load 2015 financial data into db */
load data infile '/var/lib/mysql-files/Group41/2015_Financial_Data_processed_G41.csv' ignore into table FinancialsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Move 2015 financial data into correct location */
insert into Financials(symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)
select symbol, 2015, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth
from FinancialsTemp;
/* Empty the temp table */
truncate FinancialsTemp;

/* Load 2016 financial data into db */
load data infile '/var/lib/mysql-files/Group41/2016_Financial_Data_processed_G41.csv' ignore into table FinancialsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Move 2016 financial data into correct location */
insert into Financials(symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)
select symbol, 2016, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth
from FinancialsTemp;
/* Empty the temp table */
truncate FinancialsTemp;

/* Load 2017 financial data into db */
load data infile '/var/lib/mysql-files/Group41/2017_Financial_Data_processed_G41.csv' ignore into table FinancialsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Move 2017 financial data into correct location */
insert into Financials(symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)
select symbol, 2017, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth
from FinancialsTemp;
/* Empty the temp table */
truncate FinancialsTemp;

/* Load 2018 financial data into db */
load data infile '/var/lib/mysql-files/Group41/2018_Financial_Data_processed_G41.csv' ignore into table FinancialsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Move 2018 financial data into correct location */
insert into Financials(symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)
select symbol, 2018, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth
from FinancialsTemp;
/* Empty the temp table */
truncate FinancialsTemp;

drop table FinancialsTemp;

/* Add missing stock tickers to Stock */
insert ignore into Stock (symbol)
select distinct symbol
from Financials;

/* Set up the foreign key restraint on symbol */
alter table Financials
add foreign key (symbol) references Stock(symbol);

/********************************************************************************************/

/********************************************************************************************/
/* LOAD IPO DATA */

/* create IPODetails Temp table to store raw date values */
create table IPODetailsTemp (
    symbol varchar(6) unique,
    year int,
    month int,
    day int,
    daysGreen int,
    daysBetterThanSP int,
    marketMonthTrend float,
    market3MonthTrend float,
    market6MonthTrend float,
    marketYearTrend float,
    usaCompany varchar(7)
);

/* create Final IPODetails table */

create table IPODetails (
    symbol varchar(6),
    day datetime,
    daysBetterThanSP int,
    daysGreen int,
    marketMonthTrend float,
    market3MonthTrend float,
    market6MonthTrend float,
    marketYearTrend float,
    usaCompany varchar(7),

    primary key (symbol, day)
);

/* Load raw data into temp table */
load data infile '/var/lib/mysql-files/Group41/IPODataFull_processed_G41.csv' ignore into table IPODetailsTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;

/* calculate date-time field and tranfer data to final table */
insert into IPODetails(symbol, day, daysBetterThanSP, daysGreen, marketMonthTrend, market3MonthTrend, market6MonthTrend, marketYearTrend, usaCompany)
select symbol, str_to_date(CONCAT(year,'-',lpad(month,2,'00'),'-',lpad(day,2,'00')), '%Y-%m-%d'), daysBetterThanSP, daysGreen, marketMonthTrend, market3MonthTrend, market6MonthTrend, marketYearTrend, usaCompany
from IPODetailsTemp;

drop table if exists IPODetailsTemp;

insert ignore into Stock (symbol)
select distinct symbol
from IPODetails;

alter table IPODetails
    add foreign key (symbol) references Stock(symbol);

/********************************************************************************************/

/********************************************************************************************/
/* LOAD ARTICLE DATA */

create table Article (
	articleID int not null,
	symbol varchar(6),
	url varchar(2048),
    headline varchar(512),
    date datetime,
    
    primary key (articleID)
);

create table AnalystArticle (
	articleID int not null,
	analyst varchar(255),
    
    primary key (articleID),
    foreign key (articleID) references Article(articleID)
);

create table PartnerArticle (
	articleID int not null,
    partner varchar(255),
    
	primary key (articleID),
    foreign key (articleID) references Article(articleID)
);

/* Create temp table for loading the csvs */
create table ArticleTemp (
	id int not null auto_increment,
	headline varchar(512),
    url varchar(2048),
    author varchar(255),
    date datetime,
    symbol varchar(6),
    type int,
    
    primary key (id)
);

/* Load the analyst articles */
load data infile '/var/lib/mysql-files/18-Stocks/raw_analyst_ratings.csv' ignore into table ArticleTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;
/* Remove invisible characters and set article type */
update ArticleTemp
set symbol = replace(symbol, '\r', ''), type = 0
where type is null;

/* Insert the analyst data into the relevant tables */
insert into Article(articleID, symbol, url, headline, date)
select id, symbol, url, headline, date
from ArticleTemp
where type = 0;

insert into AnalystArticle(articleID, analyst)
select id, author
from ArticleTemp
where type = 0;

/* Load the partner articles */
load data infile '/var/lib/mysql-files/18-Stocks/raw_partner_headlines.csv' ignore into table ArticleTemp
fields terminated by ','
enclosed by '"'
lines terminated by '\n'
ignore 1 lines;

/* Remove invisible characters and set article type */
update ArticleTemp
set symbol = replace(symbol, '\r', ''), type = 1
where type is null;

/* Insert the partner data into the relevant tables */
insert ignore into Article(articleID, symbol, url, headline, date)
select id, symbol, url, headline, date
from ArticleTemp
where type = 1;

insert into PartnerArticle(articleID, partner)
select id, author
from ArticleTemp
where type = 1;


/* Add missing stock tickers to Stock */
insert ignore into Stock (symbol)
select distinct symbol
from Article;

/* Set up the foreign key restraint on symbol and auto increment PK */
alter table AnalystArticle
drop foreign key AnalystArticle_ibfk_1;
alter table PartnerArticle
drop foreign key PartnerArticle_ibfk_1;

alter table Article
add foreign key (symbol) references Stock(symbol),
modify articleID int auto_increment;

alter table AnalystArticle
add foreign key (articleID) references Article(articleID) on delete cascade;
alter table PartnerArticle
add foreign key (articleID) references Article(articleID) on delete cascade;

/* Drop the temp table as we no longer need it */
drop table ArticleTemp;

/********************************************************************************************/

/********************************************************************************************/
/* CREATE STOCK COMMENT */
/* create StockCommnet table */
create table StockComment (
    symbol varchar(6),
    date datetime,
    text text,

    primary key (symbol, date),
    foreign key (symbol) references Stock(symbol)
);

/* no need to load as client will add data */

/********************************************************************************************/
