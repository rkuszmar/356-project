import mysql.connector
from search_queries import *
from insert_queries import *
from common_functions import *

def individual(db, cursor):
    print(SEPARATOR + "\nEntered individual mode\nType \"help\" for additional instructions\n" + SEPARATOR)
    active_ticker = None

    while True:
        print("Active ticker: <" + ("NONE SELECTED" if active_ticker is None else active_ticker) + ">")
        user_input = input().lower()

        # Check user input and go to appropriate command function
        if user_input == COMMAND_INDIVIDUAL_HELP:
            individual_help()

        elif user_input == COMMAND_INSERT_STOCK:
            insert_stock_query(db, cursor)

        elif user_input == COMMAND_INSERT_PRICE:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                insert_price_query(user_input, db, cursor, active_ticker)

        elif user_input == COMMAND_INSERT_FINANCIALS:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                insert_financials_query(user_input, db, cursor, active_ticker)

        elif user_input == COMMAND_INSERT_IPO:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                insert_ipo_query(user_input, db, cursor, active_ticker)

        elif user_input == COMMAND_INSERT_COMMENT:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                insert_comment_query(user_input, db, cursor, active_ticker)

        elif user_input == COMMAND_INSERT_ARTICLE:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                insert_article_query(user_input, db, cursor, active_ticker)

        elif user_input.startswith(COMMAND_INDIVIDUAL_SEARCH):
            ticker = user_input.removeprefix(COMMAND_INDIVIDUAL_SEARCH).upper()
            cursor.execute("select symbol from Stock where symbol = '" + ticker + "'")
            valid = True if len(cursor.fetchall()) > 0 else False # Check if the ticker is valid by referencing the database
            if valid:
                active_ticker = ticker
            else:
                print("Ticker <" + ticker + "> is invalid.")

        elif user_input == COMMAND_INDIVIDUAL_IPO:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                ipo_query(user_input, db, cursor, active_ticker)

        elif user_input.startswith(COMMAND_INDIVIDUAL_FINANCIALS):
            if active_ticker is None:
              print (INDIVIDUAL_MISSING_TICKER)
            else:
                financials_query(user_input, db, cursor, active_ticker)

        elif user_input.startswith(COMMAND_INDIVIDUAL_PRICING):
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                pricing_query(user_input, db, cursor, active_ticker)

        elif user_input.startswith(COMMAND_INDIVIDUAL_ARTICLE):
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                article_query(user_input, db, cursor, active_ticker)

        elif user_input.startswith(COMMAND_INDIVIDUAL_COMMENT):
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                comment_query(user_input, db, cursor, active_ticker)

        elif user_input == COMMAND_INDIVIDUAL_BACK:
            print(SEPARATOR + "\nLeaving \"individual\" mode\n" + SEPARATOR)
            break

        elif user_input == COMMAND_INDIVIDUAL_DELETE_STOCK:
            if active_ticker is None:
                print(INDIVIDUAL_MISSING_TICKER)
            else:
                delete_stock_query(db, cursor, active_ticker)
                active_ticker = None

        else:
            print("Unknown command, type \"help\" for instructions")

def individual_help():
    print('"search <ticker>" to select a stock')
    print('"price <dd-mm-yyyy>" to see pricing data')
    print('"ipo" to see IPO data')
    print('"financials <year>" to see financial data')
    print('"comment <start-date> <end-date>" to see comments')
    print('"article <analyst/partner> <start-date> <end-date>" to see articles')
    print('"insert stock" to insert a new stock')
    print('"insert price" to insert pricing data')
    print('"insert ipo" to insert IPO data')
    print('"insert financials" to insert financial data')
    print('"insert comment" to insert a new comment')
    print('"insert article" to insert a new article')