import mysql.connector
from datetime import datetime
from constants import *

# Create a datetime given a formatted string
def create_datetime(dateString):
    split = dateString.split('-')
    day = int(split[0])
    month = int(split[1])
    year = int(split[2])

    return datetime(year, month, day)

# Collect input for price data
def collect_price_input():
    date = input("Date (dd-mm-yyyy): ")
    volume = input("Volume: ")
    open = input("Open: ")
    high = input("High: ")
    low = input("Low: ")
    close = input("Close: ")
    adjClose = input("Adjusted close: ")

    return (create_datetime(date), volume, open, high, low, close, adjClose)

# Collect input for ipo data
def collect_ipo_input():
    date = input("Date (dd-mm-yyyy): ")
    daysBetterThanSP = input("Days better than S&P: ")
    daysGreen = input("Days green: ")
    monthTrend = input("First month trend: ")
    month3Trend = input("First 3-month trend: ")
    month6Trend = input("First 6-month trend: ")
    yearTrend = input("First year trend: ")
    usaCompany = input("Usa company: (YES or NO): ")

    return (create_datetime(date), daysBetterThanSP, daysGreen, monthTrend, month3Trend, month6Trend, yearTrend, usaCompany)

# Collect input for financials data
def collect_financials_input():
    revenue = input("Revenue: ")
    revenueGrowth = input("Revenue growth: ")
    costOfRevenue = input("Cost of revenue: ")
    grossProfit = input("Gross profit: ")
    rndExpenses = input("R&D expenses: ")
    eps = input("EPS: ")
    epsDiluted = input("EPS diluted: ")
    dividendPerShare = input("Divident per share: ")
    ebitda = input("ebitda: ")
    ebit = input("ebit: ")
    profitMargin = input("Profit margin: ")
    cashAndEquivalent = input("Cash and equivalents: ")
    debtGrowth = input("Debt growth: ")

    return (revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted,
            dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth)

# Collect input for comment dat
def collect_comment_input():
    text = input("Text: ")

    return (text,)

# Collect input for article data
def collect_article_input(articleType):
    url = input("URL: ")
    headline = input("Headline: ")
    date = input("Date (dd-mm-yyyy): ")

    if articleType is None:
        articleType = input("Type (analyst or partner): ").lower()
    
    authorType = "Analyst: " if articleType == "analyst" else "Partner: "
    author = input(authorType)

    return (url, headline, create_datetime(date), articleType, author) 