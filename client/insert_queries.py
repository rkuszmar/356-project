import mysql.connector
from datetime import datetime
from constants import *
from common_functions import *

# Insert a stock ticker into the strong entity table
def insert_stock_query(db, cursor):
    user_input = input("Stock symbol: ")

    sql = "insert into Stock (symbol) values (%s)"
    val = [(user_input.upper())]
    cursor.execute(sql, val)
    db.commit()

    print(INSERT_DATA)

# Insert a row into the stock price table
def insert_price_query(user_input, db, cursor, active_ticker):
    val = (active_ticker,) + collect_price_input()

    sql = "insert into StockPrice (symbol, date, volume, open, high, low, close, adjClose) values (%s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(sql, val)
    db.commit()

    print(INSERT_DATA)

# Insert a row into the financials table
def insert_financials_query(user_input, db, cursor, active_ticker):
    year = input("Year: ")
    val = (active_ticker, year) + collect_financials_input()

    sql = str("insert into Financials (symbol, year, revenue, revenueGrowth, costOfRevenue, grossProfit, rndExpenses, eps, epsDiluted, dividendPerShare, ebitda, ebit, profitMargin, cashAndEquivalent, debtGrowth) "
    + " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
    cursor.execute(sql, val)

    db.commit()
    print(INSERT_DATA)

# Insert a row into the IPODetails table 
def insert_ipo_query(user_input, db, cursor, active_ticker):
    val = (active_ticker,) + collect_ipo_input()

    sql = "insert into IPODetails (symbol, day, daysBetterThanSP, daysGreen, marketMonthTrend, market3MonthTrend, market6MonthTrend, marketYearTrend, usaCompany) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(sql, val)

    db.commit()
    print(INSERT_DATA)

# Insert a comment
def insert_comment_query(user_input, db, cursor, active_ticker):
    val = (active_ticker,) + (datetime.now(),) + collect_comment_input() 

    sql = "insert into StockComment (symbol, date, text) values (%s, %s, %s)"
    cursor.execute(sql, val)

    db.commit()
    print(INSERT_DATA)

# Insert an analyst or partner article
def insert_article_query(user_input, db, cursor, active_ticker):
    vals = (active_ticker,) + collect_article_input(None)

    # Insert into main article table
    sql = "insert into Article (symbol, url, headline, date) values (%s, %s, %s, %s)"
    val = (vals[0], vals[1], vals[2], vals[3])
    cursor.execute(sql , val)
    articleID = cursor.lastrowid

    # Insert into specialized tables
    if vals[4] == "analyst":
        sql = "insert into AnalystArticle (articleID, analyst) values (%s, %s)"
        val = (articleID, vals[5])
        cursor.execute(sql, val)
    else:
        sql = "insert into PartnerArticle (articleID, partner) values (%s, %s)"
        val = (articleID, vals[5])
        cursor.execute(sql, val)
    
    db.commit()
    print(INSERT_DATA)

# Delete all data for a given stock
def delete_stock_query(db, cursor, active_ticker):
    cursor.execute("delete from StockComment where symbol = '" + active_ticker + "'")
    cursor.execute("delete from StockPrice where symbol = '" + active_ticker + "'")
    cursor.execute("delete from IPODetails where symbol = '" + active_ticker + "'")
    cursor.execute("delete from Financials where symbol = '" + active_ticker + "'")
    cursor.execute("delete from Article where symbol = '" + active_ticker + "'")
    cursor.execute("delete from Stock where symbol = '" + active_ticker + "'")

    db.commit()

    print(INDIVIDUAL_DELETE_COMPLETE)