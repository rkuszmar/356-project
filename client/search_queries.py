import mysql.connector
from common_functions import *

# Perform a search for the ipo data on a given stock
def ipo_query(user_input, db, cursor, active_ticker):
    cursor.execute("select * from IPODetails where symbol = '" + active_ticker + "'")
    result = cursor.fetchone()
    if result is None:
        print("No IPO data available for " + active_ticker)
    else:
        print("")
        print("Symbol: " + str(result["symbol"]))
        print("Date: " + str(result["day"]))
        print("Days better than S&P during year: " + str(result["daysBetterThanSP"]))
        print("Days green during year: " + str(result["daysGreen"]))
        print("Initial month trend: " + str(result["marketMonthTrend"]))
        print("Initial 3-month trend: " + str(result["market3MonthTrend"]))
        print("Initial 6-month trend: " + str(result["market6MonthTrend"]))
        print("Initial year trend: " + str(result["marketYearTrend"]))
        print("USA company: " + str(result["usaCompany"]))
        print("")

        next_command = input(INDIVIDUAL_NEXT_COMMAND).lower()
        if next_command == "d":
            cursor.execute("delete from IPODetails where symbol = '" + active_ticker + "'")
            db.commit()
            print(INDIVIDUAL_DELETE_COMPLETE)
        elif next_command == "u":
            val = collect_ipo_input()
            sql = str("update IPODetails set day = %s, daysBetterThanSP = %s, daysGreen = %s, marketMonthTrend = %s, " + 
                        "market3MonthTrend = %s, market6MonthTrend = %s, marketYearTrend = %s, usaCompany = %s " +
                        "where symbol = '" + active_ticker + "'")
            cursor.execute(sql, val)
            db.commit()
            print(INDIVIDUAL_UPDATE_COMPLETE)

# Perform a search for the financials data on a given stock
def financials_query(user_input, db, cursor, active_ticker):
    year = user_input.removeprefix(COMMAND_INDIVIDUAL_FINANCIALS)
    cursor.execute("select * from Financials where symbol = '" + active_ticker + "' and year ='" + year + "'")
    result = cursor.fetchone()
    if result is None:
        print("No financial data available for " + active_ticker + " during the year of " + year)
    else:
        print("")
        print("Symbol: " + str(result["symbol"]))
        print("Year: " + str(result["year"]))
        print("Revenue: " + str(result["revenue"]))
        print("Revenue growth: " + str(result["revenueGrowth"]))
        print("Cost of revenue: " + str(result["costOfRevenue"]))
        print("Gross profit: " + str(result["grossProfit"]))
        print("R&D expenses: " + str(result["rndExpenses"]))
        print("EPS: " + str(result["eps"]))
        print("EPS diluted: " + str(result["epsDiluted"]))
        print("Dividend per share: " + str(result["dividendPerShare"]))
        print("EBITDA: " + str(result["ebitda"]))
        print("EBIT: " + str(result["ebit"]))
        print("Profit margin: " + str(result["profitMargin"]))
        print("Cash and Equivalents: " + str(result["cashAndEquivalent"]))
        print("Debt growth: " + str(result["debtGrowth"]))
        print("")

        next_command = input(INDIVIDUAL_NEXT_COMMAND).lower()
        if next_command == "d":
            cursor.execute("delete from Financials where symbol = '" + active_ticker + "' and year = '" + year + "'")
            db.commit()
            print(INDIVIDUAL_DELETE_COMPLETE)
        elif next_command == "u":
            val = collect_financials_input()
            sql = str("update Financials set revenue = %s, revenueGrowth = %s, costOfRevenue = %s, grossProfit = %s, " + 
                        "rndExpenses = %s, eps = %s, epsDiluted = %s, dividendPerShare = %s,  ebitda = %s, ebit = %s, " +
                        "profitMargin = %s, cashAndEquivalent = %s, debtGrowth = %s " +
                        "where symbol = '" + active_ticker + "' and year = '" + year + "'")
            cursor.execute(sql, val)
            db.commit()
            print(INDIVIDUAL_UPDATE_COMPLETE)

# Perform a search for the pricing data on a given stock
def pricing_query(user_input, db, cursor, active_ticker):
    date = user_input.removeprefix(COMMAND_INDIVIDUAL_PRICING)
    splitDate = date.split("-")
    day = splitDate[0]
    month = splitDate[1]
    year = splitDate[2]

    cursor.execute("select * from StockPrice where symbol = '" + active_ticker + "' and day(date) = '" + day + "' and month(date) = '" + month + "' and year(date) = '" + year + "'")
    result = cursor.fetchone()

    if result is None:
        print("No pricing data available for " + active_ticker + " at " + day + "-" + month + "-" + year + " (dd-mm-yyyy)")
    else:
        print("")
        print("Symbol: " + str(result["symbol"]))
        print("Date: " + str(result["date"]))
        print("Volume: " + str(result["volume"]))
        print("Open: " + str(result["open"]))
        print("High: " + str(result["high"]))
        print("Low: " + str(result["low"]))
        print("Close: " + str(result["close"]))
        print("Adjusted close: " + str(result["adjClose"]))
        print("")

        next_command = input(INDIVIDUAL_NEXT_COMMAND).lower()
        if next_command == "d":
            cursor.execute("delete from StockPrice where symbol = '" + active_ticker + "' and date = '" + str(result["date"]) + "'")
            db.commit()
            print(INDIVIDUAL_DELETE_COMPLETE)
        elif next_command == "u":
            val = collect_price_input()
            sql = str("update StockPrice set date = %s, volume = %s, open = %s, high = %s, " + 
                        "low = %s, close = %s, adjClose = %s " +
                        "where symbol = '" + active_ticker + "' and date = '" + str(result["date"]) + "'")
            cursor.execute(sql, val)
            db.commit()
            print(INDIVIDUAL_UPDATE_COMPLETE)

# Perform a search for the article data on a given stock
def article_query(user_input, db, cursor, active_ticker):
    specifiers = user_input.removeprefix(COMMAND_INDIVIDUAL_ARTICLE).split(' ')
    date1 = specifiers[1]
    date2 = specifiers[2]

    if specifiers[0].lower() == "partner":
        cursor.execute("select * from Article a inner join PartnerArticle p on a.articleID = p.articleID "
        + " where a.date >= str_to_date('" + date1 + "', '%d-%m-%Y') and a.date <= str_to_date('" + date2 + "', '%d-%m-%Y') "
        + " and a.symbol = '" + active_ticker + "' order by a.date")
        print("")

        results = cursor.fetchall()
        if len(results)  == 0:
            print("No partner articles found for " + active_ticker + " between " + date1 + " and " + date2)
            print("")
        else:
            i = 0
            for row in results:
                print("Article [" + str(i) + "]")
                print("Date: " + str(row["date"]))
                print("Partner: " + str(row["partner"]))
                print("Headline: " + str(row["headline"]))
                print("URL: " + str(row["url"]))
                print("")
                i += 1
            
            next_commands = input(INDIVIDUAL_NEXT_COMMAND_MULTI).lower().split(' ')
            if next_commands[0] == "d":
                entryIndex = int(next_commands[1])
                cursor.execute("delete from PartnerArticle where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute("delete from Article where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                db.commit()
                print(INDIVIDUAL_DELETE_COMPLETE)
            elif next_commands[0] == "u":
                entryIndex = int(next_commands[1])
                vals = collect_article_input("partner")

                sql = str("update Article set url = %s, headline = %s, date = %s " + 
                            "where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute(sql, (vals[0], vals[1], vals[2]))

                sql = str("update PartnerArticle set partner = %s " +
                            "where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute(sql, (vals[4],))

                db.commit()
                print(INDIVIDUAL_UPDATE_COMPLETE)

    elif specifiers[0].lower() == "analyst":
        cursor.execute("select * from Article a inner join AnalystArticle an on a.articleID = an.articleID "
        + " where a.date >= str_to_date('" + date1 + "', '%d-%m-%Y') and a.date <= str_to_date('" + date2 + "', '%d-%m-%Y') "
        + " and a.symbol = '" + active_ticker + "' order by a.date")
        print("")

        results = cursor.fetchall()
        if len(results)  == 0:
            print("No analyst articles found for " + active_ticker + " between " + date1 + " and " + date2)
            print("")
        else:
            i = 0
            for row in results:
                print("Article [" + str(i) + "]")
                print("Date: " + str(row["date"]))
                print("Analyst: " + str(row["analyst"]))
                print("Headline: " + str(row["headline"]))
                print("URL: " + str(row["url"]))
                print("")
                i += 1
            
            next_commands = input(INDIVIDUAL_NEXT_COMMAND_MULTI).lower().split(' ')
            if next_commands[0] == "d":
                entryIndex = int(next_commands[1])
                cursor.execute("delete from AnalystArticle where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute("delete from Article where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                db.commit()
                print(INDIVIDUAL_DELETE_COMPLETE)
            elif next_commands[0] == "u":
                entryIndex = int(next_commands[1])
                vals = collect_article_input("analyst")

                sql = str("update Article set url = %s, headline = %s, date = %s " + 
                            "where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute(sql, (vals[0], vals[1], vals[2]))

                sql = str("update AnalystArticle set analyst = %s " +
                            "where articleID = '" + str(results[entryIndex]["articleID"]) + "'")
                cursor.execute(sql, (vals[4],))

                db.commit()
                print(INDIVIDUAL_UPDATE_COMPLETE)

    else:
        print("Invalid article type selected")

# Perform a search for the comment data on a given stock
def comment_query(user_input, db, cursor, active_ticker):
    specifiers = user_input.removeprefix(COMMAND_INDIVIDUAL_ARTICLE).split(' ')
    date1 = specifiers[1]
    date2 = specifiers[2]

    cursor.execute("select * from StockComment where date >= str_to_date('" + date1 + "', '%d-%m-%Y') and date <= str_to_date('" + date2 + "', '%d-%m-%Y') " + 
        " and symbol = '" + active_ticker + "' order by date")
    print("")

    results = cursor.fetchall()
    if len(results) == 0:
        print("No comments found for " + active_ticker + " between " + date1 + " and " + date2)
        print("")
    else:
        i = 0
        for row in results:
            print("Comment [" + str(i) + "]")
            print("Date: " + str(row["date"]))
            print("Comment: " + str(row["text"]))
            print("")
            i += 1

        next_commands = input(INDIVIDUAL_NEXT_COMMAND_MULTI).lower().split(' ')
        if next_commands[0] == "d":
            entryIndex = int(next_commands[1])
            cursor.execute("delete from StockComment where symbol = '" + active_ticker + "' and date = '" + str(results[entryIndex]["date"]) + "'")
            db.commit()
            print(INDIVIDUAL_DELETE_COMPLETE)
        elif next_commands[0] == "u":
            entryIndex = int(next_commands[1])
            val = (datetime.now(),) + collect_comment_input()
            sql = str("update StockComment set date = %s, text = %s " + 
                        "where symbol = '" + active_ticker + "' and date = '" + str(results[entryIndex]["date"]) + "'")
            cursor.execute(sql, val)
            db.commit()
            print(INDIVIDUAL_UPDATE_COMPLETE)