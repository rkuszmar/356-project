import mysql.connector
from individual import *
from constants import *
from common_functions import *

def main():

    # First establish a connection to the database
    db = None
    while True:
        db = login()
        if db is None:
            print("Connection failed, try again")
        else:
            print("Successful connection")
            break

    cursor = db.cursor(buffered = True, dictionary = True)

    # Run the "interface" loop
    print("Type \"help\" for instructions")
    while True:
        user_input = input().lower()
        if user_input == COMMAND_HELP:
            help()
        elif user_input == COMMAND_INDIVIDUAL:
            individual(db, cursor)
        elif user_input == COMMAND_EXIT:
            print("Closing connection...")
            db.close()
            print("Connection closed. Goodbye!")
            break
        else:
            print("Unknown command, type \"help\" for instructions")

def login():
    host = input("Host: ")
    user = input("Username: ")
    password = input("Password: ")
    database = input("Database: ")

    try:
        return mysql.connector.connect(
            host = host,
            user = user,
            password = password,
            database = database
        )
    except:
        return None

def help():
    print('"individual" to enter that mode')
    print('"exit" to close the connection')

if __name__ == "__main__":
    main()